# GitHub Stats

## Get the code

```
git clone https://gitlab.com/fortran-lang/github_stats
```

## Installation

You can set up the package with Conda or pip.

### Conda

```
conda env create -f environment.yml
conda activate gh_stats
```

### pip

```
python3 -m venv venv
source venv/bin/activate
pip install -U pip
pip install -r requirements.txt
```

## Set up GitHub authentication

Create a file `account.toml` and place your GitHub credentials in it:

```toml
[github]
user = "your_github_ID"
token = "TOKEN"
```

To create a GitHub API token:

1. Go to https://github.com/settings/tokens
2. Click on the "Generate new token" button
3. Confirm your password
4. On the "New personal access token" page, in the "Note" field, put some note
   such as "j3 stats", no need to modify anything else (leave all defaults) and
   then click on the "Generate token" button.
5. On the next page, copy the token and put it into your `account.toml` file

The basic token is needed in order to be able to use the GitHub API.

## Running the scripts

### Downloading GitHub data

```
python download_issues.py j3-fortran/fortran_proposals
python stats.py data-j3-fortran-fortran_proposals.json
```

The `download_issues.py` script accepts the repository name and downloads all
GitHub issues and saves them into `data-j3-fortran-fortran_proposals.json`.

### Computing statistics

The script `stats.py` accepts the data json filename, loads it and creates the
statistics. It does not require GitHub, your credentials or internet access, all
the information is present in the local JSON file.

### Generating contributors data for the fortran-lang.org newsletter

For each monthly newsletter, we update the contributors data that is stored
in the fortran-lang/fortran-lang.org repository on GitHub.

To update the contributors data, first run the download script for the
following repositories:

```
python download_issues.py j3-fortran/fortran_proposals
python download_issues.py fortran-lang/stdlib
python download_issues.py fortran-lang/fpm
python download_issues.py fortran-lang/fortran-lang.org
```

This will generate a new JSON file for each repository.

Then, copy these files to the `fortran-lang.org/community/github_stats_data`
directory in your local fork of fortran-lang/fortran-lang.org.
Git add, commit, and push the new files, and the newsletter will have access to
the up-to-date contributors data.

### Generating a list of proposals from j3-fortran/fortran_proposals

Generate the GitHub token (see above). Setup your Python Conda or pip
environment (see above). Then:

    python download_issues.py j3-fortran/fortran_proposals
    python j3_issues_to_md.py

Note: the first command takes about 2.5 minutes to finish as of April 2022.

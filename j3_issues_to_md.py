from datetime import datetime, timezone
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
import sys

from model import Repository, Issue, Comment, load_json

clause_names = {
    1: "Scope",
    2: "Normative references",
    3: "Terms and definitions",
    4: "Notation, conformance, and compatibility",
    5: "Fortran concepts",
    6: "Lexical tokens and source form",
    7: "Types",
    8: "Attribute declarations and specifications",
    9: "Use of data objects",
    10: "Expressions and assignment",
    11: "Execution control",
    12: "Input/output statements",
    13: "Input/output editing",
    14: "Program units",
    15: "Procedures",
    16: "Intrinsic procedures and modules",
    17: "Exceptions and IEEE arithmetic",
    18: "Interoperability with C",
    19: "Scope, association, and definition",
}


def get_clause_number(labels):
    """
    Returns the clause number by searching labels for "Clause NN".

    Returns if no Clause label present
    """
    for l in labels:
        if l.startswith("Clause"):
            return int(l.split()[1])
    return 0


def get_issue_url(repo: str, issue_number: int) -> str:
    """
    Returns a GitHub URL for a given repo and issue number.

    repo must include both the owner/org and the repo name, for example
    j3-fortran/fortran_proposals.
    """
    url = "/".join(["https://github.com", repo, "issues", str(issue_number)])
    return url


def candidate_for_2y(labels):
    """
    Returns True if the issue does *not* have any of the following labels:

    * Fortran 202x
    * duplicate
    * meta
    * rejected

    And thus can be considered for Fortran 202Y.

    Otherwise returns False.
    """
    for l in labels:
        if l in ["Fortran 202x", "duplicate", "meta", "rejected"]:
            return False
    return True


json_file = "data-j3-fortran-fortran_proposals.json"
repo_name = "j3-fortran/fortran_proposals"

repo = load_json(json_file, Repository)
print("# List of Proposals for 202Y")
print()
issues = sorted(repo.issues, key=lambda x: x.number)
clause = {}
not_candidates = []
closed = []
for i in issues:
    issue_date = parse(i.date)
    if i.is_open:
        if candidate_for_2y(i.labels):
            c = get_clause_number(i.labels)
            l = clause.get(c, [])
            l.append(i)
            clause[c] = l
        else:
            not_candidates.append(i)
    else:
        closed.append(i)

for c in range(1, 20):
    if c in clause:
        print("## Clause %i: %s" % (c, clause_names[c]))
        for i in clause[c]:
            url = get_issue_url(repo_name, i.number)
            print("* [#%i: %s](%s)" % (i.number, i.title, url))
        print()

print("## Other")
for i in clause[0]:
    url = get_issue_url(repo_name, i.number)
    print("* [#%i: %s](%s)" % (i.number, i.title, url))
print()

print("## Issues not considered for 202Y")
for i in not_candidates:
    url = get_issue_url(repo_name, i.number)
    print("* [#%i: %s](%s)" % (i.number, i.title, url))
print()

print("## Closed issues")
for i in closed:
    url = get_issue_url(repo_name, i.number)
    print("* [#%i: %s](%s)" % (i.number, i.title, url))

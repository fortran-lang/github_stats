"""
Script to generate a list of contributors for the Fortran newsletter:

https://fortran-lang.org/news/

"""
from datetime import datetime, timezone
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
import sys

from model import Repository, Issue, Comment, load_json

# Date range
date1 = datetime(2020, 5, 1, tzinfo=timezone.utc)
date2 = datetime(2020, 5, 31, tzinfo=timezone.utc)

json_files = [
    "data-fortran-lang-stdlib.json",
    "data-fortran-lang-fpm.json",
    "data-fortran-lang-fortran-lang.org.json",
    "data-j3-fortran-fortran_proposals.json",
]

# Individual repositories statistics:

users_sum = {}
for json_file in json_files:
    print("GitHub repository information:")
    print("JSON filename: %s" % json_file)
    repo = load_json(json_file, Repository)
    print("Contributors who commented and how many times:")
    print("%-3s   %-22s %s" % ("  N", "GitHub ID", "Number of comments"))
    users = {}
    for i in repo.issues:
        comment_date = parse(i.date)
        if not (date1 <= comment_date and comment_date <= date2):
            continue
        # Count creating an issue as one comment
        user_count = users.get(i.user, 0)
        users[i.user] = user_count + 1
        user_count = users_sum.get(i.user, 0)
        users_sum[i.user] = user_count + 1
        # Count all comments posted under an issue
        for c in i.comments:
            comment_date = parse(c.date)
            if not (date1 <= comment_date and comment_date <= date2):
                continue
            user_count = users.get(c.user, 0)
            users[c.user] = user_count + 1
            user_count = users_sum.get(c.user, 0)
            users_sum[c.user] = user_count + 1
    users = sorted(users.items(), key=lambda x: x[1], reverse=True)
    for i, (u, n) in enumerate(users):
        print("%3d.  %-22s %4d" % (i+1, u, n))
    print()

# Statistics across all repositories
print("-" * 80)
print("Sum across all repositories")
del users_sum["github-actions[bot]"]
users_sum = sorted(users_sum.items(), key=lambda x: x[1], reverse=True)
for i, (u, n) in enumerate(users_sum):
    print("%3d.  %-22s %4d" % (i+1, u, n))
print()

# Markdown to be included in the newsletter itself

print("-" * 80)
print("Markdown to be included in the newsletter:")
print()
print("We thank everybody who contributed to fortran-lang in the past month by")
print("commenting in any of the four repositories")
print("[fortran-lang/stdlib](https://github.com/fortran-lang/stdlib),")
print("[fortran-lang/fpm](https://github.com/fortran-lang/fpm),")
print("[fortran-lang/fortran-lang.org](https://github.com/fortran-lang/fortran-lang.org),")
print("[j3-fortran/fortran_proposals](https://github.com/j3-fortran/fortran_proposals):")
print()

from dataclasses import dataclass
from github3 import login
from model import load_toml

@dataclass
class GitHub:
    user: str
    token: str

@dataclass
class Account:
    github: GitHub

account = load_toml("account.toml", Account)
user = account.github.user
pw = account.github.token

gh = login(user, pw)

for i, (u, n) in enumerate(users_sum):
    name = gh.user(u).name
    if name:
        print("%s ([@%s](https://github.com/%s)), " % (name, u, u),
                end="")
    else:
        print("[@%s](https://github.com/%s), " % (u, u),
                end="")
print()
